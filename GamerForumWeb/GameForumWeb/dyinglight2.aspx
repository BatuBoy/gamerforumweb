﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="dyinglight2.aspx.cs" Inherits="dyinglight2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="row">
                <div class="col-lg-8">
                    <!-- Post content-->
                    <article>
                        <!-- Post header-->
                        <header class="mb-4">
                            <!-- Post title-->
                            <h1 class="fw-bolder mb-1">Dying Light 2!</h1>
                            <!-- Post meta content-->
                            <div class="text-muted fst-italic mb-2">Posted on June 15, 2021 by BatuBoy</div>
                            <!-- Post categories-->
                            <a class="badge bg-secondary text-decoration-none link-light" href="#!">Survival</a>
                            <a class="badge bg-secondary text-decoration-none link-light" href="#!">Parkour</a>
                        </header>
                        <!-- Preview image figure-->
                        <figure class="mb-4"><img class="img-fluid rounded" src="https://cdn1.epicgames.com/salesEvent/salesEvent/EGS_DyingLight2StayHuman_Techland_S3_2560x1440-f1dcd15207f091674615ccb4bd9dc3c7?h=270&resize=1&w=480" alt="..." /></figure>
                        <!-- Post content-->
                        <section class="mb-5">
                            <p class="fs-5 mb-4">E3 is on the horizon, but that hasn't stopped a number of publisher and developers from dropping some sweet new details and information on their upcoming games - one of which is Techland, the team behind survival action RPG sequel Dying Light 2.</p>
                            <p class="fs-5 mb-4">The game will build on its predecessor's post-apocalyptic parkour vibe, which went down a treat with gamers in the initial game - scoring 'Overwhelmingly Positive' reviews on Steam. Players got to see a glimpse of what's to come in the E3 2019 world premiere gameplay trailer, which focused on the narrative of the game and how decisions that you make can cause the world around you to change drastically.</p>
                            <p class="fs-5 mb-4">Dying Light has been in development for some time, and with the recent pandemic causing havoc to pretty much all industries on the planet, it was obvious that some studios would have to delay development for a little while. But this week, Techland treated us to its 'Stay Human' trailer, which went into a bit more detail about the open-world, zombie-bashing title - as well as casually dropping an official launch date.</p>
                            <h2 class="fw-bolder mb-4 mt-5">More story details</h2>
                            <p class="fs-5 mb-4">The trailer went about introducing us to some of the characters that protagonist Aiden Caldwell will encounter along the way in the action-persuaded story. Embarking on an adventure to the big city in search of answers to the mysteries of his past, Aiden meets a group of veteran survivors called the 'Nightrunners'.

</p>
                            <p class="fs-5 mb-4">While it's insinuated that not every human survivor you come across is friendly, you'll certainly need to count on some allies for the more dangerous missions when heading into enemy territory... which is pretty much everywhere!</p>
                            <p class="fs-5 mb-4">The story takes place 15 years after the world was first brought to its knees by the apocalypse, also known as the 'Modern Dark Ages'. While many lost their lives fleeing from the chaos and roaming the streets for supplies, others to shelter above - and in Dying Light 2, a lot of the settlements that you come across will be based high above ground.


</p>
                            <p class="fs-5 mb-4">Rope bridges and zip wires will come in handy as you run, zip and zoom across buildings - as well as being nimble on the streets to avoid enemies.</p>
                            <p class="fs-5 mb-4">&nbsp;There are three main factions features in Dying Light 2, some can become allies through your actions, while others simply want nothing more than to loot your corpse - such as Bandits and Thugs. Survivors set up camps in various locations and adapt to life away from harm, while Peacekeepers act as a military type presence in the city with a new-age of law and order.

</p>
                            <p class="fs-5 mb-4">Then there's the Renegades, a group of ex criminals being ruled by an iron fist under a ruthless colonel. Depending on who you help or dismantle will see other factions thrive in new areas of the city.

</p>
                            <p class="fs-5 mb-4">It's also worth noting that Aiden himself is infected with the dreaded virus that has turned the city upside down, so you'll also have to keep that under wraps while dealing with dramas, dangers and whatever else Dying Light 2 has to throw at you.</p>
                        </section>
                    </article>
                    <!-- Comments section-->
                    <section class="mb-5">
                        <div class="card bg-light">
                            <div class="card-body">
                                <!-- Comment form-->
                                <form class="mb-4">
                                    <textarea class="form-control" id="commentArea" name="commentArea" rows="3" placeholder="Join the discussion and leave a comment!"></textarea><asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Comment" />
                                </form>
                                <!-- Comment with nested comments-->
                                <div class="d-flex mb-4">
                                    <!-- Parent comment-->
                                    <div class="flex-shrink-0"><img class="rounded-circle" src="https://dummyimage.com/50x50/ced4da/6c757d.jpg" alt="..." /></div>
                                    <div class="ms-3">
                                        <div class="fw-bold">Commenter Name</div>
                                        If you're going to lead a space frontier, it has to be government; it'll never be private enterprise. Because the space frontier is dangerous, and it's expensive, and it has unquantified risks.
                                        <!-- Child comment 1-->
                                        <div class="d-flex mt-4">
                                            <div class="flex-shrink-0"><img class="rounded-circle" src="https://dummyimage.com/50x50/ced4da/6c757d.jpg" alt="..." /></div>
                                            <div class="ms-3">
                                                <div class="fw-bold">Commenter Name</div>
                                                And under those conditions, you cannot establish a capital-market evaluation of that enterprise. You can't get investors.
                                            </div>
                                        </div>
                                        <!-- Child comment 2-->
                                        <div class="d-flex mt-4">
                                            <div class="flex-shrink-0"><img class="rounded-circle" src="https://dummyimage.com/50x50/ced4da/6c757d.jpg" alt="..." /></div>
                                            <div class="ms-3">
                                                <div class="fw-bold">Commenter Name</div>
                                                When you put money directly to a problem, it makes a good headline.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Single comment-->
                                <div class="d-flex">
                                    <div class="flex-shrink-0"><img class="rounded-circle" src="https://dummyimage.com/50x50/ced4da/6c757d.jpg" alt="..." /></div>
                                    <div class="ms-3">
                                        <div class="fw-bold">Commenter Name</div>
                                        When I look at the universe and all the ways the universe wants to kill us, I find it hard to reconcile that with statements of beneficence.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <!-- Side widgets-->
                <div class="col-lg-4">
                    <!-- Search widget-->
                    
                    <!-- Categories widget-->
                    <div class="card mb-4">
                        <div class="card-header">Most Visited Posts</div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <ul class="list-unstyled mb-0">
                                        <li><a href="#!">Dying Light 2</a></li>
                                        <li><a href="#!">Apex Legends</a></li>
                                        <li><a href="#!">Battlefield 2042</a></li>
                                    </ul>
                                </div>
                                <div class="col-sm-6">
                                    <ul class="list-unstyled mb-0">
                                        <li><a href="#!">Valorant</a></li>
                                        <li><a href="#!">Cs:Go</a></li>
                                        <li><a href="#!">League of Legends</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Side widget-->
                    <div class="card mb-4">
                        <div class="card-header">Communicate with Admins</div>
                        <div class="card-body">Send Ticket:<br />
                            <asp:TextBox ID="TextBox1" runat="server" Height="45px" Width="318px"></asp:TextBox>
                            <asp:Button ID="Button1" runat="server" Text="Send" Width="82px" />
                            <br />
                            <br />
                            </div>
                    </div>
                </div>
            </div>
</asp:Content>

