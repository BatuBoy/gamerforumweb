﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="profile_page.aspx.cs" Inherits="profile_page" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Label ID="Label1" runat="server" Text="Welcome "></asp:Label>
        <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>
        <br />
        <br />
        <br />
        Here is your informations:<br />
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="id" DataSourceID="SqlDataSource1">
            <Columns>
                <asp:BoundField DataField="id" HeaderText="id" InsertVisible="False" ReadOnly="True" SortExpression="id" />
                <asp:BoundField DataField="name_surname" HeaderText="name_surname" SortExpression="name_surname" />
                <asp:BoundField DataField="subject" HeaderText="subject" SortExpression="subject" />
                <asp:BoundField DataField="message" HeaderText="message" SortExpression="message" />
                <asp:BoundField DataField="date" HeaderText="date" SortExpression="date" />
            </Columns>
            <EmptyDataTemplate>
                Henüz hiç gönderiniz yok<br />
            </EmptyDataTemplate>
        </asp:GridView>
        <br />
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:GameForumConnection %>" ProviderName="<%$ ConnectionStrings:GameForumConnection.ProviderName %>" SelectCommand="SELECT * FROM [forum] WHERE ([name_surname] = ?)">
            <SelectParameters>
                <asp:SessionParameter Name="name_surname" SessionField="user" Type="String" />
            </SelectParameters>
        </asp:SqlDataSource>
        <br />
        <br />
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Go to Forum Page" />
        <br />
    </form>
</body>
</html>
