﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Page1.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="row">
                <!-- Blog entries-->
                <div class="col-lg-8">
                    <!-- Featured blog post-->
                    <div class="card mb-4" style="left: 0px; top: 0px">
                        <a href="#"><img class="card-img-top" src="https://cdn1.epicgames.com/salesEvent/salesEvent/EGS_DyingLight2StayHuman_Techland_S3_2560x1440-f1dcd15207f091674615ccb4bd9dc3c7?h=270&resize=1&w=480" alt="..." /></a>
                        &nbsp;<div class="card-body">
                            <div class="small text-muted">June 15, 2021</div>
                            <h2 class="card-title">Dying Light 2</h2>
                            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!</p>
                            <a class="btn btn-primary" href="#!">Read more →</a>
                        </div>
                    </div>
                    <!-- Nested row for non-featured blog posts-->
                    <div class="row">
                        <div class="col-lg-6">
                            <!-- Blog post-->
                            <div class="card mb-4">
                                <a href="#!"><img class="card-img-top" src="https://cdn1.epicgames.com/salesEvent/salesEvent/EGS_Battlefield2042_DICE_S1_2560x1440-36f16374c9c29a18a46872795b483d72?h=270&resize=1&w=480" alt="..." /></a>
                                <div class="card-body">
                                    <div class="small text-muted">June 8, 2021</div>
                                    <h2 class="card-title h4">Battlefield 2042</h2>
                                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla.</p>
                                    <a class="btn btn-primary" href="#!">Read more →</a>
                                </div>
                            </div>
                            <!-- Blog post-->
                            <div class="card mb-4">
                                <a href="#!"><img class="card-img-top" src="https://images8.alphacoders.com/992/992724.png" alt="..." /></a>
                                <div class="card-body">
                                    <div class="small text-muted">May 21, 2021</div>
                                    <h2 class="card-title h4">Apex Legends</h2>
                                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla.</p>
                                    <a class="btn btn-primary" href="#!">Read more →</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <!-- Blog post-->
                            <div class="card mb-4">
                                <a href="#!"><img class="card-img-top" src="https://wallpapercave.com/wp/wp5825925.jpg" alt="..." /></a>
                                <div class="card-body">
                                    <div class="small text-muted">May 18, 2021</div>
                                    <h2 class="card-title h4">CoD Warzone</h2>
                                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla.</p>
                                    <a class="btn btn-primary" href="#!">Read more →</a>
                                </div>
                            </div>
                            <!-- Blog post-->
                            <div class="card mb-4">
                                <a href="#!"><img class="card-img-top" src="https://wallpapercave.com/wp/wp3805945.jpg" alt="..." /></a>
                                <div class="card-body">
                                    <div class="small text-muted">May 11, 2021</div>
                                    <h2 class="card-title h4">Civilization VI</h2>
                                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam.</p>
                                    <a class="btn btn-primary" href="#!">Read more →</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Pagination-->
                    <nav aria-label="Pagination">
                        <hr class="my-0" />
                        <ul class="pagination justify-content-center my-4">
                            <li class="page-item disabled"><a class="page-link" href="#" tabindex="-1" aria-disabled="true">Newer</a></li>
                            <li class="page-item active"><a class="page-link" href="#!">1</a></li>
                            <li class="page-item"><a class="page-link" href="Page2.aspx">2</a></li>
                            <li class="page-item"><a class="page-link" href="Page3.aspx">3</a></li>                                          
                            <li class="page-item disabled"><a class="page-link" href="#!" aria-disabled="true">Older</a></li>
                        </ul>
                    </nav>
                </div>
                <!-- Side widgets-->
                <div class="col-lg-4">
                    <!-- Search widget-->
                    
                    <!-- Categories widget-->
                    <div class="card mb-4">
                        <div class="card-header">Most Visited Posts</div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <ul class="list-unstyled mb-0">
                                        <li><a href="#!">Dying Light 2</a></li>
                                        <li><a href="#!">Apex Legends</a></li>
                                        <li><a href="#!">Battlefield 2042</a></li>
                                    </ul>
                                </div>
                                <div class="col-sm-6">
                                    <ul class="list-unstyled mb-0">
                                        <li><a href="#!">Valorant</a></li>
                                        <li><a href="#!">Cs:Go</a></li>
                                        <li><a href="#!">League of Legends</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Side widget-->
                    <div class="card mb-4">
                        <div class="card-header">Communicate with Admins</div>
                        <div class="card-body">Send Ticket:<br />
                            <asp:TextBox ID="TextBox1" runat="server" Height="45px" Width="318px"></asp:TextBox>
                            <asp:Button ID="Button1" runat="server" Text="Send" Width="82px" />
                            <br />
                            <br />
                            </div>
                    </div>
                </div>
            </div>
</asp:Content>

