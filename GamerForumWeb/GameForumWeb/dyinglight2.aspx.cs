﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.OleDb;

public partial class dyinglight2 : System.Web.UI.Page
{
    OleDbConnection con = new OleDbConnection(@"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = C:\Users\BatuBoy\Desktop\Gamer Forum Web\GamerForumWeb\GameForumWeb\App_Data\Database.accdb");
    OleDbCommand cmd = new OleDbCommand();
    OleDbDataReader rd;

    protected void Page_Load(object sender, EventArgs e)
    {
        Label label = (Label)Master.FindControl("Label1");
        label.Text = "Welcome " + (string)Session["user"] + "!";
        Label2.Text = this.GetType().Name;
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        string userComment = Request.Form["commentArea"];
        string user = (string)Session["user"];
        string post = this.GetType().Name;
        string date = DateTime.Today.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();

        con.Open();
        cmd = new OleDbCommand("INSERT INTO Comments (User, Comment, Post, Date) VALUES (?, ?, ?)", con);
        
        cmd.Parameters.AddWithValue("@User", user);
        cmd.Parameters.AddWithValue("@Comment", userComment);
        cmd.Parameters.AddWithValue("@Post", post);
        cmd.Parameters.AddWithValue("@Date", date);
        con.Close();
    }
}