﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Page3.aspx.cs" Inherits="Page3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="row">
                <!-- Blog entries-->
                <div class="col-lg-8">           
                    <!-- Nested row for non-featured blog posts-->
                    <div class="row">
                        <div class="col-lg-6">
                            <!-- Blog post-->
                            <div class="card mb-4">
                                <a href="#!"><img class="card-img-top" src="https://www.webtekno.com/images/editor/default/0002/03/a0ae949ee228b20c193ef59ff4ebc01590894e8c.jpeg" alt="..." /></a>
                                <div class="card-body">
                                    <div class="small text-muted">June 8, 2021</div>
                                    <h2 class="card-title h4">League of Legends</h2>
                                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla.</p>
                                    <a class="btn btn-primary" href="#!">Read more →</a>
                                </div>
                            </div>
                            <!-- Blog post-->
                            <div class="card mb-4">
                                <a href="#!"><img class="card-img-top" src="https://wallpapercave.com/wp/wp4884170.jpg" alt="..." /></a>
                                <div class="card-body">
                                    <div class="small text-muted">May 21, 2021</div>
                                    <h2 class="card-title h4">Age of Empires II</h2>
                                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla.</p>
                                    <a class="btn btn-primary" href="#!">Read more →</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <!-- Blog post-->
                            <div class="card mb-4">
                                <a href="#!"><img class="card-img-top" src="https://zoccos.com/wp-content/uploads/2019/08/7oxe2g7t0y411.jpg" alt="..." /></a>
                                <div class="card-body">
                                    <div class="small text-muted">May 18, 2021</div>
                                    <h2 class="card-title h4">Counter Strike:Global Offensive</h2>
                                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla.</p>
                                    <a class="btn btn-primary" href="#!">Read more →</a>
                                </div>
                            </div>
                            <!-- Blog post-->
                            <div class="card mb-4">
                                <a href="#!"><img class="card-img-top" src="https://i.ytimg.com/vi/F9tni2Z-T0w/maxresdefault.jpg" alt="..." /></a>
                                <div class="card-body">
                                    <div class="small text-muted">May 11, 2021</div>
                                    <h2 class="card-title h4">Grand Theft Auto V</h2>
                                    <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam.</p>
                                    <a class="btn btn-primary" href="#!">Read more →</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Pagination-->
                    <nav aria-label="Pagination">
                        <hr class="my-0" />
                        <ul class="pagination justify-content-center my-4">
                            <li class="page-item disabled"><a class="page-link" href="#" tabindex="-1" aria-disabled="true">Newer</a></li>
                            <li class="page-item "><a class="page-link" href="Page1.aspx">1</a></li>
                            <li class="page-item"><a class="page-link" href="Page2.aspx">2</a></li>
                            <li class="page-item active"><a class="page-link" href="#!">3</a></li>                                          
                            <li class="page-item disabled"><a class="page-link" href="#!" aria-disabled="true">Older</a></li>
                        </ul>
                    </nav>
                </div>
                <!-- Side widgets-->
                <div class="col-lg-4">
                    <!-- Categories widget-->
                    <div class="card mb-4">
                        <div class="card-header">Most Visited Posts</div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <ul class="list-unstyled mb-0">
                                        <li><a href="#!">Dying Light 2</a></li>
                                        <li><a href="#!">Apex Legends</a></li>
                                        <li><a href="#!">Battlefield 2042</a></li>
                                    </ul>
                                </div>
                                <div class="col-sm-6">
                                    <ul class="list-unstyled mb-0">
                                        <li><a href="#!">Valorant</a></li>
                                        <li><a href="#!">Cs:Go</a></li>
                                        <li><a href="#!">League of Legends</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Side widget-->
                    <div class="card mb-4">
                        <div class="card-header">Communicate with Admins</div>
                        <div class="card-body">Send Ticket:<br />
                            <asp:TextBox ID="TextBox1" runat="server" Height="45px" Width="318px"></asp:TextBox>
                            <asp:Button ID="Button1" runat="server" Text="Send" Width="82px" />
                            <br />
                            <br />
                            </div>
                    </div>
                </div>
            </div>
</asp:Content>

